const OTPEmail = require("./OTPEmail");
const ResetPasswordEmail = require("./ResetPasswordEmail");
const VerifyEmail = require("./VerifyEmail");

module.exports = {
  OTPEmail,
  ResetPasswordEmail,
  VerifyEmail,
};
