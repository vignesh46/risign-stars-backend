/**
 * WARNING: do not update this values,
 * if any changes required in functional privileges
 * make the functional privileges as object,
 * keys should remain the same, values can then be updated
 * keys will be used for application,
 * values can be used for displaying in ui
 */
const BUSINESS_DEFINITION = "BUSINESS_DEFINITION";
const CLASS_ATTENDANCE = "CLASS_ATTENDANCE";
const CLASS_CATEGORY = "CLASS_CATEGORY";
const CLASS_DEFINITION = "CLASS_DEFINITION";
const CLASS_ENROLMENT = "CLASS_ENROLMENT";
const EVALUATION_SCHEME = "EVALUATION_SCHEME";
const MEMBERS = "MEMBERS";
const PROGRESS_RECORD = "PROGRESS_RECORD";
const SESSION_DEFINITION = "SESSION_DEFINITION";
const SESSION_TERM = "SESSION_TERM";
const USERS = "USERS";
const ROLES = "ROLES";

module.exports = {
  BUSINESS_DEFINITION,
  CLASS_ATTENDANCE,
  CLASS_CATEGORY,
  CLASS_DEFINITION,
  CLASS_ENROLMENT,
  EVALUATION_SCHEME,
  MEMBERS,
  PROGRESS_RECORD,
  SESSION_DEFINITION,
  SESSION_TERM,
  USERS,
  ROLES,
};
