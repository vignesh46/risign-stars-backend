const CLUB_MEMBERSHIP_ID = "CLUB_MEMBERSHIP_ID";

const ENUM_COUNTER_TYPES = [CLUB_MEMBERSHIP_ID];

module.exports = {
  ENUM_COUNTER_TYPES,
  CLUB_MEMBERSHIP_ID,
};
