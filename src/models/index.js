const Bill = require("./Bill");
const Business = require("./business");
const BusinessClass = require("./businessClass");
const BusinessSession = require("./businessSession");
const Category = require("./Category");
const Counter = require("./Counter");
const Enrolment = require("./Enrolment");
const EvaluationScheme = require("./evaluation");
const Member = require("./Member");
const MemberConsent = require("./MemberConsent");
const Progress = require("./progress");
const Role = require("./Role");
const Term = require("./Term");
const User = require("./User");

module.exports = {
  Bill,
  Business,
  BusinessClass,
  BusinessSession,
  Category,
  Counter,
  Enrolment,
  EvaluationScheme,
  Member,
  MemberConsent,
  Progress,
  Role,
  Term,
  User,
};
