const findFunctionalPrivilegesIndex = require("./findFunctionalPrivileges");
const getRoleIds = require("./getRoleIds");
const getRoles = require("./getRoles");
const hasAllPermission = require("./hasAllPermission");
const hasFunctionalPrivileges = require("./hasFunctionalPrivileges");
const hasPermission = require("./hasPermission");

module.exports = {
  findFunctionalPrivilegesIndex,
  getRoleIds,
  getRoles,
  hasAllPermission,
  hasFunctionalPrivileges,
  hasPermission,
};
