const newEnrolment = require("./newEnrolment");
const withdrawEnrolment = require("./withdrawEnrolment");
const transferEnrolment = require("./transferEnrolment");
const updateWaitlistEnrolment = require("./updateWaitlistEnrolment");
const trailEnrolment = require("./trailEnrolment");

module.exports = {
  newEnrolment,
  withdrawEnrolment,
  transferEnrolment,
  trailEnrolment,
  updateWaitlistEnrolment,
};
