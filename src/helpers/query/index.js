const getQuery = require("./getQuery");
const getOptions = require("./getOptions");

module.exports = { getQuery, getOptions };
